import React, { Component } from 'react';
import WelcomePage from './page/WelcomePage';

class App extends Component {
  render() {
    return (
      <div>
        <WelcomePage />
      </div>
    );
  }
}

export default App;
