import React from "react";
import "./styles.scss";

function RadioInput({ labelText, inputId }) {
  return (
    <div className="form__radio-group">
      <input
        className="form__radio-input"
        type="radio"
        id={inputId}
        name="size"
      />
      <label className="form__radio-label" htmlFor={inputId}>
        <span className="form__radio-button" />
        {labelText}
      </label>
    </div>
  );
}

export default RadioInput;
