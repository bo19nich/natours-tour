import React from "react";
import "./styles.scss";

function InputText({ inputName, inputType, inputId }) {
  return (
    <div className="form__group">
      <input
        type={inputType}
        placeholder={inputName}
        id={inputId}
        required
        className="form__input"
      />
      <label htmlFor={inputId} className="form__label">
        {inputName}
      </label>
    </div>
  );
}

export default InputText;
