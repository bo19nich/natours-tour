import React from "react";
import FeatureBox from "../FeatureBox";
import "./styles.scss";

function Feature() {
  return (
    <section className="section-feature">
      <div className="row">
        <div className="col-1-of-4">
          <FeatureBox
            iconClass="lnr-earth"
            heading="Explore the world"
            paragraph="Lorem ipsum dolor sit amet consectetur, adipisicing elit. Minus enim
          amet expedita sequi nulla unde."
          />
        </div>
        <div className="col-1-of-4">
          <FeatureBox
            iconClass="lnr-map-marker"
            heading="Meet nature"
            paragraph="Lorem ipsum dolor sit amet consectetur, adipisicing elit. Minus enim
          amet expedita sequi nulla unde."
          />
        </div>
        <div className="col-1-of-4">
          <FeatureBox
            iconClass="lnr-map"
            heading="Find your war"
            paragraph="Lorem ipsum dolor sit amet consectetur, adipisicing elit. Minus enim
          amet expedita sequi nulla unde."
          />
        </div>
        <div className="col-1-of-4">
          <FeatureBox
            iconClass="lnr-heart"
            heading="Live a healthier life"
            paragraph="Lorem ipsum dolor sit amet consectetur, adipisicing elit. Minus enim
          amet expedita sequi nulla unde."
          />
        </div>
      </div>
    </section>
  );
}

export default Feature;
