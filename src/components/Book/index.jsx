import React from "react";
import InputText from "../Form/InputText";
import RadioInput from "../Form/RadioInput";
import "./styles.scss";

const Book = () => {
  return (
    <section className="section-book">
      <div className="row">
        <div className="book">
          <div className="book__form">
            <form action="#" className="form">
              <div className="u-margin-bottom-small">
                <h2 className="heading-secondary">start booking now</h2>
              </div>
              <InputText
                inputName="Full Name"
                inputType="text"
                inputId="name"
              />
              <InputText
                inputName="Email address"
                inputType="email"
                inputId="email"
              />
              <div className="form__group u-margin-bottom-medium">
                <RadioInput labelText="Small group tour" inputId="small" />
                <RadioInput labelText="Large group tour" inputId="large" />
              </div>
              <div className="form_group">
                <button className="btn btn--green">next step &rarr;</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Book;
