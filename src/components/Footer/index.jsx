import React from "react";
import "./styles.scss";

function Footer() {
  return (
    <div className="footer">
      <div className="footer__logo-box">
        <img
          className="footer__logo-img"
          alt="footer-logo"
          src={require("../../assets/img/logo-green-2x.png")}
        />
      </div>
      <div className="row">
        <div className="col-1-of-2">
          <div className="footer__navigation">
            <ul className="footer__list">
              <li className="footer__item">
                <a href="#" className="footer__link">
                  Company
                </a>
              </li>
              <li className="footer__item">
                <a href="#" className="footer__link">
                  Contact us
                </a>
              </li>
              <li className="footer__item">
                <a href="#" className="footer__link">
                  Carrers
                </a>
              </li>
              <li className="footer__item">
                <a href="#" className="footer__link">
                  Privacy policy
                </a>
              </li>
              <li className="footer__item">
                <a href="#" className="footer__link">
                  Terms
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="col-1-of-2">
          <p className="footer__copy-right">
            Built by{" "}
            <a href="#" className="footer__link">
              Bonny Nicholas
            </a>{" "}
            for his online course{" "}
            <a href="#" className="footer__link">
              Advanced CSS and Sass
            </a>
            . Copyright &copy; by Jonas Schmedtmann. Lorem ipsum dolor sit amet
            consectetur adipisicing elit. Alias, ipsam. Obcaecati, expedita ipsa
            quam blanditiis in facilis qui! Qui quod delectus reiciendis
            inventore natus aut consequuntur labore, blanditiis eveniet aliquam?
          </p>
        </div>
      </div>
    </div>
  );
}

export default Footer;
