import React from "react";
import "./styles.scss";

function About() {
  return (
    <section className="section-about">
      <div className="u-center-text u-margin-bottom-big">
        <h2 className="heading-secondary">
          Exciting tours for adventurous people
        </h2>
      </div>
      <div className="row">
        <div className="col-1-of-2">
          <h3 className="heading-tertiary u-margin-bottom-small">
            You're going to fall in love with nature
          </h3>
          <p className="paragraph">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quibusdam
            cupiditate laboriosam reprehenderit tenetur libero odio saepe et
            modi exercitationem, quia, fuga id delectus ipsam. Et magnam ab quo
            unde hic?
          </p>
          <h3 className="heading-tertiary u-margin-bottom-small">
            Live adventures like you never before
          </h3>
          <p className="paragraph">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Minus enim
            amet expedita sequi nulla unde.
          </p>
          <a href="#" className="btn-text">
            Learn more &rarr;
          </a>
        </div>
        <div className="col-1-of-2">
          <div className="composition">
            <img
              src={require("../../assets/img/nat-1-large.jpg")}
              alt="photo1"
              className="composition__photo composition__photo--p1"
            />
            <img
              src={require("../../assets/img/nat-2-large.jpg")}
              alt="photo2"
              className="composition__photo composition__photo--p2"
            />
            <img
              src={require("../../assets/img/nat-3-large.jpg")}
              alt="photo3"
              className="composition__photo composition__photo--p3"
            />
          </div>
        </div>
      </div>
    </section>
  );
}

export default About;
