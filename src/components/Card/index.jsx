import React from "react";
import "./styles.scss";

const renderList = list => {
  return list.map((item, i) => <li key={i}>{item}</li>);
};

function Card({ heading, detailsList, spanClass, classPic, classBack, price }) {
  return (
    <div className="card">
      <div className="card__side card__side--front">
        <div className={`card__picture ${classPic}`}>&nbsp;</div>
        <h4 className="card__heading">
          <span className={`card__heading-span ${spanClass}`}>{heading}</span>
        </h4>
        <div className="card__details">
          <ul>{renderList(detailsList)}</ul>
        </div>
      </div>
      <div className={`card__side card__side--back ${classBack}`}>
        <div className="card__cta">
          <div className="card__price-box">
            <p className="card__price-only">only</p>
            <p className="card__price-value">${price}</p>
          </div>
          <a href="#popup" className="btn btn--white">
            Book now!
          </a>
        </div>
      </div>
    </div>
  );
}

export default Card;
