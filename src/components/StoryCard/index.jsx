import React from "react";
import "./styles.scss";

function StoryCard({ name, heading, story, img }) {
  return (
    <div className="story">
      <figure className="story__shape">
        <img
          src={require(`../../assets/img/${img}`)}
          alt="person on tour"
          className="story__img"
        />
        <figcaption className="story__caption">{name}</figcaption>
      </figure>
      <div className="story__text">
        <h3 className="heading-tertiary u-margin-bottom-small">{heading}</h3>
        <p>{story}</p>
      </div>
    </div>
  );
}

export default StoryCard;
