import React from "react";
import "./styles.scss";

export default function Header() {
  return (
    <header className="header">
      <div className="header__logo-box">
        <img
          src={require("../../assets/img/logo-white.png")}
          alt="logo"
          className="header__logo"
        />
      </div>

      <div className="header__text-box">
        <h1 className="heading-primary">
          <span className="heading-primary--main">Outdoors</span>
          <span className="heading-primary--sub">is where life happens</span>
        </h1>
        <a href="#section-tour" className="btn btn--white btn--animation">
          discover our tours
        </a>
      </div>
    </header>
  );
}
