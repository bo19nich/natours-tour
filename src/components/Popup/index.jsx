import React from "react";
import "./styles.scss";

function Popup() {
  return (
    <div className="popup" id="popup">
      <div className="popup__content">
        <div className="popup__left">
          <img
            className="popup__img"
            src={require("../../assets/img/nat-8.jpg")}
            alt="Tour_photo"
          />
          <img
            className="popup__img"
            src={require("../../assets/img/nat-9.jpg")}
            alt="Tour_photo"
          />
        </div>
        <div className="popup__right">
          <a href="#section-tour" className="popup__close">
            &times;
          </a>
          <h2 className="heading-secondary u-margin-bottom-small">
            Start booking now
          </h2>
          <h3 className="heading-tertiary u-margin-bottom-small">
            {" "}
            Important &ndash; Please read these terms before booking
          </h3>
          <p className="popup__text">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime
            itaque numquam aliquid debitis amet, mollitia expedita officiis,
            dolorem, fuga corporis tenetur eum! Consequuntur eveniet delectus
            natus placeat iusto, vitae cum.
          </p>
          <a href="#" className="btn btn--green">
            Book now
          </a>
        </div>
      </div>
    </div>
  );
}

export default Popup;
