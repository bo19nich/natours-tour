import React from "react";
import "./styles.scss";

function FeatureBox({ heading, paragraph, iconClass }) {
  return (
    <div className="feature-box">
      <span className={`lnr ${iconClass}  feature-box__icon`}></span>
      <h3 className="heading-tertiary u-margin-bottom-small">{heading}</h3>
      <p className="feature-box__text">{paragraph}</p>
    </div>
  );
}

export default FeatureBox;
