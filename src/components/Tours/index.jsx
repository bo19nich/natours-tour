import React from "react";
import Card from "../Card";
import "./styles.scss";

function Tours() {
  return (
    <section className="section-tour" id="section-tour">
      <div className="u-center-text u-margin-bottom-big">
        <h2 className="heading-secondary">Most popular tours</h2>
      </div>
      <div className="row">
        <div className="col-1-of-3">
          <Card
            heading="the sea explore"
            spanClass="card__heading-span--1"
            classPic="card__picture--1"
            classBack="card__side--back-1"
            price="279"
            detailsList={[
              "3 Day tour",
              "Up to 30 people",
              "2 tour guides",
              "sleep in cozy hotels",
              "Difficulty: easy"
            ]}
          />
        </div>
        <div className="col-1-of-3">
          <Card
            heading="the forest hiker"
            spanClass="card__heading-span--2"
            classPic="card__picture--2"
            classBack="card__side--back-2"
            price="479"
            detailsList={[
              "7 Day tour",
              "Up to 40 people",
              "6 tour guides",
              "sleep in provided tents",
              "Difficulty: medium"
            ]}
          />
        </div>
        <div className="col-1-of-3">
          <Card
            heading="the snow adventurer"
            spanClass="card__heading-span--3"
            classPic="card__picture--3"
            classBack="card__side--back-3"
            price="879"
            detailsList={[
              "5 Day tour",
              "Up to 15 people",
              "3 tour guides",
              "sleep in provided tents",
              "Difficulty: hard"
            ]}
          />
        </div>
      </div>

      <div className="u-center-text u-margin-top-big">
        <a href="#" className="btn btn--green">
          discover all tours
        </a>
      </div>
    </section>
  );
}

export default Tours;
