import React from "react";
import StoryCard from "../StoryCard";
import "./styles.scss";

function Stories() {
  return (
    <section className="section-stories">
      <div className="bg-video">
        <video className="bg-video__content" autoPlay muted loop>
          <source
            src={require("../../assets/img/video.mp4")}
            type="video/mp4"
          />
          <source
            src={require("../../assets/img/video.webm")}
            type="video/webm"
          />
          Your browser is not supported!
        </video>
      </div>
      <div className="u-center-text u-margin-bottom-big">
        <h2 className="heading-secondary">we make people genuinely happy</h2>
      </div>
      <div className="row">
        <StoryCard
          story="Lorem ipsum dolor, sit amet consectetur adipisicing elit. Deleniti,
        natus ipsam ea, iste ex inventore reprehenderit possimus iure quia
        aspernatur, consequatur laboriosam? Nisi accusantium, voluptatibus
        laudantium debitis atque molestiae quo aspernatur, consequatur
        laboriosam? Nisi accusantium, voluptatibus laudantium debitis atque
        molestiae quo!"
          name="marry smith"
          heading="i had the best weekend ever with my family"
          img="nat-8.jpg"
        />

        <StoryCard
          story="Lorem ipsum dolor, sit amet consectetur adipisicing elit. Deleniti,
        natus ipsam ea, iste ex inventore reprehenderit possimus iure quia
        aspernatur, consequatur laboriosam? Nisi accusantium, voluptatibus
        laudantium debitis atque molestiae quo aspernatur, consequatur
        laboriosam? Nisi accusantium, voluptatibus laudantium debitis atque
        molestiae quo!"
          name="jack wilson"
          img="nat-9.jpg"
          heading="Wow! My life is completely different now."
        />

        <div className="u-center-text u-margin-top-big">
          <a href="#" className="btn-text">
            Discover all tours &rarr;
          </a>
        </div>
      </div>
    </section>
  );
}

export default Stories;
