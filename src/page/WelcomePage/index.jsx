import React from "react";
import About from "../../components/About";
import Book from "../../components/Book";
import Feature from "../../components/Feature";
import Footer from "../../components/Footer";
import Header from "../../components/Header";
import Navigation from "../../components/Navigation";
import Popup from "../../components/Popup";
import Stories from "../../components/Stories";
import Tours from "../../components/Tours";

function WelcomePage() {
  return (
    <div>
      <Navigation />
      <Header />
      <main>
        <About />
        <Feature />
        <Tours />
        <Stories />
        <Book />
      </main>
      <Footer />
      <Popup />
    </div>
  );
}

export default WelcomePage;
